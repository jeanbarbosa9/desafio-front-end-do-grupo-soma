import React from 'react';
import { BrowserRouter } from 'react-router-dom'

import AppProvider from './hooks';

import Routes from './routes';

import Header from './layouts/Header';
import Footer from './layouts/Footer';

import GlobalStyles from './styles/global';


function App() {
  return (
    <AppProvider>
      <GlobalStyles/>
      <BrowserRouter>
        <Header />
        <Routes />
        <Footer />
      </BrowserRouter>
    </AppProvider>
  )
}

export default App;
