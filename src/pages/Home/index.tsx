import React from 'react';

import banner1Src from '../../assets/home_banner_1.png';
import banner2Src from '../../assets/home_banner_2.png';
import banner3Src from '../../assets/home_banner_3.png';
import banner4Src from '../../assets/home_banner_4.png';
import banner5Src from '../../assets/home_banner_5.png';
import banner6Src from '../../assets/home_banner_6.png';
import banner7Src from '../../assets/home_banner_7.png';

import {
  Container,
  ImageInfoContainer,
  InfoTitle,
  InfoDescription,
  InfoCallToAction,
  InfoContainer,
} from './styles';

const Home: React.FC = () => (
  <Container>
    <img src={banner1Src} alt='banner 1' />
    <img src={banner2Src} alt='banner 2' />
    <img src={banner3Src} alt='banner 3' />
    <img src={banner4Src} alt='banner 4' />
    <div>
      <ImageInfoContainer>
        <img src={banner5Src} alt='banner 5' />
        <InfoContainer>
          <InfoTitle>Lunar</InfoTitle>
          <InfoDescription>
            De Sevilla, capital do flamenco, o vestuário típico da dança mais celebrada da Espanha inspira as novas peças da coleção.
          </InfoDescription>
          <InfoCallToAction>shop now</InfoCallToAction>
        </InfoContainer>
      </ImageInfoContainer>
      <ImageInfoContainer>
        <img src={banner6Src} alt='banner 6' />
        <InfoContainer>
          <InfoTitle>Flutua</InfoTitle>
          <InfoDescription>
            A natureza dos célebres jardins espanhóis dá vida às estampas florais.
          </InfoDescription>
          <InfoCallToAction>shop now</InfoCallToAction>
        </InfoContainer>
      </ImageInfoContainer>
      <ImageInfoContainer>
        <img src={banner7Src} alt='banner 7' />
        <InfoContainer>
          <InfoTitle>Flash Lights</InfoTitle>
          <InfoDescription>
            O último edital também se inspira em Barcelona, cidade iluminada pelo sol. Conheça as cores que são aposta da coleção.
          </InfoDescription>
          <InfoCallToAction>shop now</InfoCallToAction>
        </InfoContainer>
      </ImageInfoContainer>
    </div>
  </Container>
)

export default Home;
