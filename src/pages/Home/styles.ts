import styled from 'styled-components';

import { colors, breakpoints } from '../../styles/variables';

const { greyDark, primary } = colors;
const { tablet_sm, desk_xs } = breakpoints;

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  > div {
    display: flex;
    width: 100%;
    justify-content: center;

    @media only screen and (max-width: ${tablet_sm}px) {
      flex-wrap: wrap;
    }

    > * {
      @media only screen and (min-width: 640px) {
        width: calc(100% / 2);
      }
    }

    @media only screen and (min-width: ${desk_xs}px) {
      > * {
        width: calc(100% / 3);
      }
    }
  }

  img {
    max-width: 100%;
  }
`;

export const ImageInfoContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;

`;

export const InfoContainer = styled.div`
  display: flex;
  max-width: 70%;
  padding: 15px;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  p {
    margin-bottom: 15px;
  }
`;

export const InfoTitle = styled.p`
  font-size: 20px;
  text-transform: uppercase;
`;

export const InfoDescription = styled.p`
  padding-bottom: 20px;
  font-size: 12px;
  color: ${greyDark()};
  text-align: center;
`;

export const InfoCallToAction = styled.span`
  position: absolute;
  bottom: 20px;
  left: 50%;
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 700;
  border-bottom: 2px solid ${primary};
  transform: translateX(-50%);
`
