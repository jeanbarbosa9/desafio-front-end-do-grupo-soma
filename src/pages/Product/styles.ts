import styled from 'styled-components';

import { colors, breakpoints } from '../../styles/variables';

const { greyLight } = colors;
const { mobile_lg } = breakpoints;

export const Container = styled.div`
  display: flex;
  padding-bottom: 50px;

  @media only screen and (max-width: ${mobile_lg}px) {
    flex-direction: column;
  }
`;

export const BreadCrumbWrapper = styled.div`
  padding: 10px 45px ;
  background-color: ${greyLight};

  @media only screen and (max-width: ${mobile_lg}px) {
    padding: 10px 0;
  }
`;
