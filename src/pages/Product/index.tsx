import React, { useState, useEffect, useCallback } from 'react';
import { useLocation } from 'react-router-dom';

import { useProducts } from '../../hooks/productsList';
import { useLoading } from '../../hooks/loading';
import { useDevice } from '../../hooks/device';
import api from '../../services/api';

import ProductProps from '../../interfaces/IProduct';

import LoadingPlaceholder from '../../components/LoadingPlaceholder';
import BreadCrumb,  { PathProps } from '../../components/BreadCrumb';
import Images from './Images';
import Infos from './Infos';

import { Container, BreadCrumbWrapper } from './styles';

const Product: React.FC = () => {
  const { pathname } = useLocation();
  const {
    allProducts,
    setAllProducts,
    findProduct,
    getProductCategory,
  } = useProducts();
  const { setIsLoading } = useLoading();
  const { isMobile } = useDevice();

  const [data, setData] = useState<ProductProps>({} as ProductProps);
  const [itemIndex, setItemIdex] = useState(0);
  const [breadCrumbPaths, setBreadCrumbPaths] = useState<PathProps[]>([
    { text: 'home', link: '/' }
  ]);
  const [imagePlaceholderCount, setImagePlaceholderCount] = useState(1)

  useEffect(() => {
    setImagePlaceholderCount(isMobile ? 1 : 2);
  }, [isMobile])

  useEffect(() => {
    const path = pathname.split('/')[1];

    async function getProducts() {
      setIsLoading(true)
      const { data } = await api.get('/products.json');

      setTimeout(() => {
        setAllProducts(data);
        setIsLoading(false)
      }, 1500)
    };

    if (!allProducts.length) {
      getProducts();
    }

    const product = findProduct(path);
    setData(product);
    const productCategory = product && getProductCategory(product.linkText);
    setBreadCrumbPaths(
      (state) => [...state, { text: productCategory, link: ''}]
    )

  }, [
      findProduct,
      pathname,
      setAllProducts,
      setIsLoading,
      allProducts,
      getProductCategory,
    ])

  const changeItemIndex = useCallback(
    (index) => {
      setItemIdex(index)
    },
    [],
  )

  return (
    <React.Fragment>
      <BreadCrumbWrapper>
        {
          data
            ? (<BreadCrumb paths={breadCrumbPaths} />)
            : (
                <LoadingPlaceholder
                  itemsPerLine={1}
                  itemHeightPx={30}
                  placeholderCount={1}
                />
            )
        }
      </BreadCrumbWrapper>
      <Container>
        {
          data
            ? (
                <React.Fragment>
                  {
                    data.items
                      ? (<Images images={data.items[itemIndex].images} />)
                      : <React.Fragment />
                  }
                  <Infos
                    callback={changeItemIndex}
                    name={data.productName}
                    referenceId={`${data.productReference}_${itemIndex + 1}`}
                    items={data.items}
                    composition={data.Composição || ''}
                    measuresTable={data["Tabela de Medidas"] || []}
                    description={data.description || ''}
                  />
                </React.Fragment>
            )
            : (
              <React.Fragment>
                <LoadingPlaceholder
                  placeholderCount={5}
                  itemHeightPx={505}
                  itemsPerLine={imagePlaceholderCount}
                />
                <LoadingPlaceholder
                  placeholderCount={1}
                  itemHeightPx={553}
                  itemsPerLine={1}
                />
              </React.Fragment>
            )
        }
      </Container>
    </React.Fragment>
  )
};

export default Product;
