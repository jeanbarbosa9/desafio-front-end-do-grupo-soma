import styled from "styled-components";

import { colors } from '../../../../styles/variables';

const { primary, greyDark, greyLight } = colors;

export const Container = styled.table`
  width: 100%;
  border: 1px solid ${primary};
  border-collapse: collapse;

  thead {
    th {
      background-color: ${greyLight}
    }

    &:not(:first-of-type) {
      th {
        border-top: 1px solid ${primary};
      }
    }
  }

  tbody {
    tr {
      &:first-of-type {
        td {
          border-top: 1px solid ${primary};
        }
      }

      &:not(:last-of-type) {
        td {
          border-bottom: 1px solid ${primary};
        }
      }
    }
  }

  th, td {
    padding: 5px;
    font-size: 11px;
    color: ${greyDark()};
  }

  td {
    + td {
      border-left: 1px solid ${primary};
    }
  }
`;
