import React from 'react';

import { Container } from './styles';

interface MeasureProps {
  [key: string]: string;
}

interface MeasureTableProps {
  measuresTable: MeasureProps[];
}

const MeasuresTable: React.FC<MeasureTableProps> = ({ measuresTable }) => {

  return (
    <Container>
      {
        measuresTable.map((measure: MeasureProps, indexThead) => (
          <React.Fragment key={indexThead}>
            <thead>
              <tr>
                <th colSpan={2}>
                  {Object.keys(measure)[0]}
                </th>
              </tr>
            </thead>
            <tbody>
              {
                Object.values(measure)[0].split(', ').map((row: string, indexTbody) => (
                  <tr key={indexTbody}>
                    {
                      row.split(' = ').map((cell: string, index) => (
                        <td key={index}>
                          {
                            index === 1
                              ? `${cell.replace('.', ',')} cm`
                              : cell
                          }
                        </td>
                      ))
                    }
                  </tr>
                ))
              }
            </tbody>
          </React.Fragment>
        ))
      }
    </Container>
  )
}

export default MeasuresTable;
