import React, { useState, useCallback, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { FiStar, FiExternalLink } from 'react-icons/fi'

import { useCart } from '../../../hooks/cart';

import { Item, Installment } from '../../../interfaces/IProduct';
import { formatMoney } from '../../../helpers/formatNumber';
import getBestItemInstallment from '../../../helpers/getBestItemInstallment';

import Button from '../../../components/Button';
import MeasuresTable  from './MeasuresTable';

import { colors } from '../../../styles/variables';

import {
  Container,
  ProductReference,
  ProductName,
  ProductPrice,
  SpecificationsWrapper,
  ColorThumbWrapper,
  ColorLabel,
  ColorValue,
  SizeLabel,
  SizeValue,
  LinksWrapper,
  ButtonWrapper,
  ProductDetailsContainer,
  DetailWrapper,
  DetailWithOpenBehavior,
  MeasuresWrapper,
  DescriptionWrapper,
  FlexWrapper,
} from './styles';

interface InfosProps {
  callback: Function;
  name: string;
  referenceId: string;
  items: Item[];
  composition: string;
  measuresTable: string[];
  description: string;
}

interface Measure {
  [key: string]: string
}

const Infos: React.FC<InfosProps> = ({
  callback,
  name,
  referenceId,
  items,
  composition,
  measuresTable,
  description,
}) =>
{
  const { pathname } = useLocation();
  const { addCartProduct } = useCart();

  const [productName, setProductName] = useState(name);
  const [selectedItem, setSelectedItem] = useState<null | number>(null);
  const [installment, setInstallment] = useState({} as Installment);
  const [item, setItem] = useState(() => items && items[selectedItem || 0]);
  const [itemLastImage, setItemLastImage] = useState('');
  const [showMeasures, setShowMeasures] = useState<Measure[]>([]);
  const [openedDetail, setOpenedDetail] = useState('');

  const itemSizes = items && items.map((item, index) => ({
    size: item.Tamanho[0],
    index,
  }))

  useEffect(() => {
    setProductName(name);
  }, [name])

  useEffect(() => {
    const installment = getBestItemInstallment(item);
    installment && setInstallment(installment)
    if (selectedItem !== null) {
      setProductName(items && items[selectedItem].nameComplete);
    }
    setItemLastImage(items && items[0].images[items[0].images.length - 1].imageUrl);
  }, [items, selectedItem, item])

  useEffect(() => {
    const parsedMeasuresTable = measuresTable[0]
      && JSON.parse(measuresTable[0]);

      parsedMeasuresTable
        && parsedMeasuresTable.map((measure: Measure) => {
          if (!!Object.values(measure)[0]) {
            return setShowMeasures((state) => [...state, measure]);
          }

          return false
        })
  }, [measuresTable])

  const handleSelectItem = useCallback(
    (index: number) => {
      callback(index);
      setSelectedItem(index);
      setItem(items[index]);
    },
    [callback, items],
  )

  const itemInterestRate = useCallback(() => {
    if (installment?.Value) {
      return installment?.InterestRate > 0 ? `com juros de ${Math.round(installment?.InterestRate)}%` : 'sem juros';
    } else {
      return false
    }
  }, [installment]);

  const itemPrice = useCallback(() => {
    if (items && items[selectedItem || 0].sellers[0].commertialOffer.Price) {
      return formatMoney(items[selectedItem || 0].sellers[0].commertialOffer.Price);
    } else {
      return false
    }
  }, [selectedItem, items])

  const handleAddToCart = useCallback(
    () => {
      const item = items && items[selectedItem || 0]

      addCartProduct({
        id: item && item.itemId,
        name: item && item.nameComplete,
        imageUrl: item && item.images[0].imageUrl,
        price: item && item.sellers[0].commertialOffer.Price
      })

      window.scrollTo({ top: 0, behavior: 'smooth'});

    },
    [selectedItem, addCartProduct, items],
  )

  const handleOpenDetail = useCallback((detail: string) => {
    if (detail === openedDetail) {
      setOpenedDetail('');
    } else {
      setOpenedDetail(detail);
    }
  }, [openedDetail]);

  return (
    <Container>
      <div>
        <ProductReference>Ref. {referenceId}</ProductReference>
        <ProductName>{productName}</ProductName>
        <ProductPrice>
          {itemPrice() && (<strong>{itemPrice()}</strong>)}
          {
            itemInterestRate()
              && (
                <span>
                  ou {installment?.NumberOfInstallments} x de {formatMoney(installment?.Value)} {itemInterestRate()}
                </span>
              )
          }
        </ProductPrice>
        <SpecificationsWrapper>
          <div>
            <ColorLabel>Cor</ColorLabel>
            <ColorValue>
              {
                items && items[0].images
                  && (
                    <ColorThumbWrapper>
                      <div>
                        <img
                          src={itemLastImage}
                          alt='Thumb da cor'
                        />
                      </div>
                    </ColorThumbWrapper>
                  )
              }
            </ColorValue>
            <SizeLabel>Tamanho</SizeLabel>
            <SizeValue>
              {
                itemSizes && itemSizes.map(item => (
                    <button
                      key={item.index}
                      onClick={() => handleSelectItem(item.index)}
                      data-is-selected={item.index === selectedItem}
                      disabled={items && !items[item.index].sellers[0].commertialOffer.AvailableQuantity}
                      title={
                        items && !items[item.index].sellers[0].commertialOffer.AvailableQuantity
                          ? 'Tamanho indisponível'
                          : `Tamanho ${item.size}`
                      }
                    >
                      {item.size}
                    </button>
                ))
              }
            </SizeValue>
          </div>
        </SpecificationsWrapper>
        <LinksWrapper>
          <a
            href={`https://www.animale.com.br${pathname}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            provador virtual
          </a>
          <a
            href={`https://www.animale.com.br${pathname}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            guia de tamanhos
          </a>
        </LinksWrapper>
        <ButtonWrapper>
          <Button
            darkButton={true}
            disabled={selectedItem === null}
            onClick={handleAddToCart}
          >Adicionar à sacola</Button>
        </ButtonWrapper>
        <ProductDetailsContainer>
          <DetailWrapper
            data-is-open={openedDetail === 'composition'}
          >
            <DetailWithOpenBehavior
              onClick={() => handleOpenDetail('composition')}
              data-is-open={openedDetail === 'composition'}
            >
              Composição
            </DetailWithOpenBehavior>
            <div><p>{composition}</p></div>
          </DetailWrapper>
          {
            showMeasures.length
              ? (
                <DetailWrapper
                  data-is-open={openedDetail === 'measures'}
                >
                  <DetailWithOpenBehavior
                    onClick={() => handleOpenDetail('measures')}
                    data-is-open={openedDetail === 'measures'}
                  >
                    Medidas dessa peça
                  </DetailWithOpenBehavior>
                  <MeasuresWrapper>
                    <MeasuresTable measuresTable={showMeasures} />
                  </MeasuresWrapper>
                </DetailWrapper>
              )
              : <React.Fragment />
          }
          {
            description && (
              <DetailWrapper
              data-is-open={openedDetail === 'description'}
              >
                <DetailWithOpenBehavior
                  onClick={() => handleOpenDetail('description')}
                  data-is-open={openedDetail === 'description'}
                >
                  Descrição
                </DetailWithOpenBehavior>
                <DescriptionWrapper>
                  <p dangerouslySetInnerHTML={{__html: description}} />
                </DescriptionWrapper>
              </DetailWrapper>
            )
          }
          <FlexWrapper>
            <a
              href={`https://www.animale.com.br${pathname}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              Como cuidar deste produto <FiExternalLink size={12} />
            </a>
          </FlexWrapper>
          <FlexWrapper>
            <p>Dúvidas e avaliações</p>
            <div>
              <FiStar size={12} fill={colors.primary} />
              <FiStar size={12} fill={colors.primary} />
              <FiStar size={12} fill={colors.primary} />
              <FiStar
                size={12}
                color={colors.grey}
                fill={colors.grey}
              />
              <FiStar
                size={12}
                color={colors.grey}
                fill={colors.grey}
              />
            </div>
          </FlexWrapper>
        </ProductDetailsContainer>
      </div>
    </Container>
  )
}

export default Infos;
