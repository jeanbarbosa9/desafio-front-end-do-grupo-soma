import styled from 'styled-components';

import { colors, transitionTimingFunction, breakpoints } from '../../../styles/variables';

const { primary, secondary, grey, greyDark } = colors;
const { desk_xs } = breakpoints;

export const Container = styled.div`
  display: flex;
  padding-top: 30px;
  width: 100%;
  height: 100%;
  justify-content: center;

  @media only screen and (min-width: ${desk_xs}px) {
    padding-top: 70px;
  }

  > div {
    display: flex;
    max-width: 315px;
    flex-direction: column;
  }
`;

export const ProductName = styled.h1`
  margin-bottom: 10px;
  font-size: 16px;
  font-weight: 500;
`;

export const ProductReference = styled.span`
  margin-bottom: 5px;
  font-size: 10px;
  color: ${greyDark()};
`;

export const ProductPrice = styled.p`
  margin-bottom: 45px;
  font-size: 12px;

  span {
    margin-left: 15px;
  }
`;

export const SpecificationsWrapper = styled.div`
  > div {
    display: grid;
    grid-template-areas:
      'colorLabel colorValue'
      'sizeLabel sizeValue';
  }
`;

export const ColorLabel = styled.span`
  display: flex;
  grid-area: colorLabel;
  padding: 12px 0;
  align-items: center;
  font-size: 12px;
  color: ${greyDark()};
`;

export const ColorValue = styled.div`
  padding: 12px 0;
  grid-area: colorValue;
`;

export const ColorThumbWrapper = styled.div`
  flex: 0 1 50%;

  > div {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    overflow: hidden;
  }

  img {
    max-width: 100%;
  }
`;

export const SizeLabel = styled.span`
  display: flex;
  grid-area: sizeLabel;
  padding: 12px 0;
  padding-right: 48px;
  align-items: center;
  font-size: 12px;
  color: ${greyDark()};
  border-top: 1px solid ${grey};
`;

export const SizeValue = styled.div`
  display: flex;
  grid-area: sizeValue;
  padding: 12px 0;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  border-top: 1px solid ${grey};

  button {
    padding: 5px;
    transition: background-color ${transitionTimingFunction}, color ${transitionTimingFunction};

    &:hover,
    &[data-is-selected="true"] {
      color: ${secondary};
      background-color: ${primary};
    }

    &:disabled {
      color: ${secondary};
      background-color: ${grey};
      cursor: not-allowed;
    }
  }
`;

export const LinksWrapper = styled.div`
  display: flex;
  padding: 20px;
  justify-content: space-around;
  align-items: center;

  a {
    color: ${primary};
    text-transform: uppercase;
    font-size: 10px;

    + a {
      margin-left: 20px;
    }
  }
`;

export const ButtonWrapper = styled.div`
  padding: 24px 0;

  button {
    width: 100%;
    padding: 12px;
    font-weight: 700;
  }
`;

export const ProductDetailsContainer = styled.div`
  padding-top: 53px;
`;

export const DetailWrapper = styled.div`
  padding: 10px 0;

  &:not(:first-of-type) {
    border-top: 1px solid ${grey};
  }

  p, a {
    position: relative;
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    font-size: 11px;
    color: ${greyDark()};
    text-decoration: none;
  }

  > div {
    height: 0;
    opacity: 0;
    visibility: hidden;
    overflow: hidden;
    transition: opacity ${transitionTimingFunction};
  }

  &[data-is-open='true'] {
    > div {
      height: auto;
      padding-top: 10px;
      opacity: 1;
      visibility: visible;
    }
  }
`;

export const FlexWrapper = styled.div`
  padding: 10px 0;

  &:not(:first-of-type) {
    border-top: 1px solid ${grey};
  }

  a, & {
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    font-size: 11px;
    color: ${greyDark()};
    text-decoration: none;
  }
`;

export const MeasuresWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const DetailWithOpenBehavior = styled.p`
  cursor: pointer;

  &:before, &:after {
    content: '';
    position: absolute;
    top: 50%;
    left: calc(100% - 10px);
    display: block;
    width: 10px;
    height: 1px;
    background-color: ${primary};
    transition: transform ${transitionTimingFunction};
  }

  &:before {
    transform: translate(-50%, -50%);
  }

  &:after {
    transform: translate(-50%, -50%) rotate(90deg);
  }

  &[data-is-open='true'] {
    padding: 10px 0;

    &:after {
      transform: translate(-50%, -50%);
    }
  }
`;

export const DescriptionWrapper = styled.div`
  p {
    display: block;
    font-size: 12px;
    line-height: 20px;
  }
`;
