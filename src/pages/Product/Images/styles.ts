import styled from 'styled-components';

import { colors, breakpoints, transitionTimingFunction } from '../../../styles/variables';

const { lightTransparence } = colors;
const { mobile_lg } = breakpoints;

export const Container = styled.div`
  position: relative;

  @media only screen and (min-width: ${mobile_lg}px) {
    display: grid;
    max-width: 50%;
    grid-template-columns: repeat(2, 1fr);
  }

  img {
    max-width: 100%;
  }

  div {
    position: relative;
    height: calc(100vw * 1.5);

    img {
      position: absolute;
      top: 0;
      left: 0;
      opacity: 0;
      visibility: hidden;
      transition: opacity ${transitionTimingFunction}, visibility ${transitionTimingFunction};

      &[data-show="true"] {
        opacity: 1;
        visibility: visible;
      }
     }
  }

  button {
    position: absolute;
    top: 50%;
    padding: 10px;
    background-color: ${lightTransparence};
    transform: translateY(-50%);
    z-index: 1;


    &[data-goto="prev"] {
      left: 10px;
    }

    &[data-goto="next"] {
      right: 10px;
    }
  }
`;
