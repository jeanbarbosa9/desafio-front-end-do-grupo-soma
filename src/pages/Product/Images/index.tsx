import React, { useState, useCallback } from 'react';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';

import { useDevice } from '../../../hooks/device';

import { colors } from '../../../styles/variables';

import { Container } from './styles';

interface Item {
  images: [{
    imageUrl: string;
  }];
}

const Images: React.FC<Item> = ({ images }) => {
  const { isMobile } = useDevice();

  const [imageIndex, setImageIndex] = useState(0);

  const handleChangeImage = useCallback(
    (type: 'next' | 'prev') => {
      if (type === 'next' && imageIndex < images.length - 2) {
        setImageIndex(imageIndex + 1)
      } else if(type === 'prev' && imageIndex > 0) {
        setImageIndex(imageIndex - 1);
      }
    },
    [imageIndex, images],
  )

  return (
    <Container>
      {
        isMobile
          ? (
            <React.Fragment>
              <button
                onClick={() => handleChangeImage('prev')}
                data-goto="prev"
              >
                <FiChevronLeft size={20} color={colors.primary} />
              </button>
              <div>
                {
                  images.map((image, index) => (
                    <img
                      src={image.imageUrl}
                      alt={`Imagem ${index + 1} do produto`}
                      data-show={imageIndex >= index}
                      key={index}
                    />
                  ))
                }

              </div>
              <button
                onClick={() => handleChangeImage('next')}
                data-goto="next"
              >
                <FiChevronRight size={20} color={colors.primary} />
              </button>
            </React.Fragment>
          )
          : images.length
            && images.map(
              (image, index, arr) => {
                if (index < arr.length - 1) {
                  return (
                    <img
                      key={index}
                      src={image.imageUrl}
                      alt={`Imagem ${index + 1} do produto`}
                    />
                  )
                }

                return <React.Fragment key={index} />
              }
            )
      }
    </Container>
  )
}

export default Images;
