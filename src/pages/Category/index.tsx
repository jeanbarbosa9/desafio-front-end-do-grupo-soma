import React, { useEffect, useState, useCallback } from 'react';

import api from '../../services/api';
import Shelf from '../../components/Shelf';
import bannerSrc from '../../assets/banner.png';
import Button from '../../components/Button';

import { useProducts } from '../../hooks/productsList';
import { useLoading } from '../../hooks/loading';

import Product from '../../interfaces/IProduct';

import { Container, Banner } from './styles';

const Category: React.FC = () => {
  const { isLoading, setIsLoading } = useLoading();
  const {
    products,
    setProducts,
    filteredProducts,
    allProducts,
    setAllProducts
  } = useProducts();

  const productsStep = 20;
  const [page, setPage] = useState(1);
  const [productsEnd, setProductsEnd] = useState(productsStep);
  const [shelfProducts, setShelfProducts] = useState<Product[]>([]);

  useEffect(() => {
    if (filteredProducts.length) {
      setShelfProducts(filteredProducts)
    } else {
      setShelfProducts(products);
    }
  }, [filteredProducts, products])

  useEffect(() => {
    async function getProducts() {
      setIsLoading(true)
      const { data } = await api.get('/products.json');

      setTimeout(() => {
        setAllProducts(data);
        setIsLoading(false)
      }, 1500)
    };

    getProducts();
  }, [setIsLoading, setAllProducts])

  useEffect(() => {
    setProducts(allProducts.slice(0, productsEnd));
  }, [allProducts, productsEnd, setProducts])

  useEffect(() => {
    setProductsEnd(productsStep * page);
  }, [page])

  const loadMoreProducts = useCallback(
    async () => {
      const newScrollY = window.scrollY + 200;

      await setIsLoading(true);
      window.scrollTo(0, newScrollY);

      setTimeout(() => {
        setPage(page + 1);
        setIsLoading(false);
      }, 1500)
    },
    [page, setPage, setIsLoading]
  );

  return (
    <Container>
      <Banner
        src={bannerSrc}
        alt="Banner da coleção de couro"
        title="Coleção de couro"
      />
      <Shelf products={shelfProducts} />
      {
        products.length && !isLoading && !filteredProducts.length
          ? products.length < allProducts.length && (
            <Button
              darkButton={false}
              onClick={loadMoreProducts}
            >Ver mais produtos</Button>
          )
          : ''
      }

    </Container>
  );
};

export default Category;
