import styled from 'styled-components';

export const Container = styled.div`
  padding-bottom: 50px;

  > button {
    display: block;
    margin: 0 auto;
    padding: 15px;
  }
`;

export const Banner = styled.img`
  max-width: 100%;
`;


