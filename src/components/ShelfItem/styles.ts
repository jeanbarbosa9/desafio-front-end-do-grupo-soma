import styled, { css } from 'styled-components';

import { colors } from '../../styles/variables';

interface ImagesProps {
  isHovering: boolean;
}

const { green, red } = colors;

export const Images = styled.div<ImagesProps>`
  position: relative;

  img {
    max-width: 100%;
    -webkit-image-rendering: -webkit-optimazeQuality;
    transition: opacity 500ms ease-in-out;

    &:last-child {
      position: absolute;
      top: 0;
      left: 0;
      opacity: 0;
      visibility: hidden;
    }
  }

  ${(props) => (
      props.isHovering && css`
        img {

          &:last-child {
            opacity: 1;
            visibility: visible;
          }
        }
      `
    )}
`;

export const Infos = styled.div`
  padding: 20px 20px 60px 32px;

  p {
    margin-bottom: 6px;

    strong + strong,
    strong + span,
    span + span,
    span + strong {
      margin-left: 30px;
    }
  }

  button {
    width: 104px;
    font-size: 12px;
  }
`;

export const ProductName = styled.p`
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  &:hover {
    white-space: normal;
  }
`;

export const PriceAndInstallments = styled.p`
  font-size: 14px;
`;

export const Discount = styled.strong`
  font-size: 12px;
  color: ${green};
`;

export const Unavailable = styled.p`
  color: ${red};
`;
