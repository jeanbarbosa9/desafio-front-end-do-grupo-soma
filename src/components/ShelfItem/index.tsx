import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';

import { formatMoney } from '../../helpers/formatNumber';
import getBestItemInstallment from '../../helpers/getBestItemInstallment';
import ProductProps from '../../interfaces/IProduct';

import Button from '../Button';

import {
  Images,
  Infos,
  ProductName,
  PriceAndInstallments,
  Discount,
  Unavailable
} from './styles';

const ShelfItem: React.FC<ProductProps> = ({
  productName, items, linkText
}) => {
  const history = useHistory()
  const [imageHovering, setImageHovering] = useState(false);

  const itemIndex = items.findIndex(item => item.sellers && item.sellers[0].commertialOffer.Price > 0)
  const item = items[itemIndex];

  const itemImages = item && item.images?.map(image => {
    const img = new window.Image();
    img.src = image.imageUrl
    return img;
  })

  const goToProductPage = useCallback(() => {
    history.push(`${linkText}/p`);
    window.scrollTo(0,0)
  }, [linkText, history])

  const installment = item && getBestItemInstallment(item)

  return item
    ? (
      <div>
        {
          itemImages && (
            <Images isHovering={imageHovering}>
              <img
                src={itemImages[0].src}
                alt={`Imagem de ${item.nameComplete}`}
                title={item.nameComplete}
                onMouseEnter={() => setImageHovering(true)}
              />
              <img
                src={itemImages[1].src}
                alt={`Imagem de ${item.nameComplete}`}
                title={item.nameComplete}
                onMouseLeave={() => setImageHovering(false)}
              />
            </Images>
          )
        }
        <Infos>
          <ProductName>{productName}</ProductName>
          {
            item.sellers
            && item.sellers[0].commertialOffer.AvailableQuantity
              ? (
                <React.Fragment>
                  <PriceAndInstallments>
                    <strong>{formatMoney(item.sellers[0].commertialOffer.Price)}</strong>
                    { installment && (
                      <span>{installment?.NumberOfInstallments}x de {formatMoney(installment?.Value || 0)}</span>
                    ) }
                  </PriceAndInstallments>
                  <p>
                    <Discount>
                      {Math.round((item.sellers[0].commertialOffer.Price / item.sellers[0].commertialOffer.ListPrice)*100)}%
                      <span> de desconto!</span>
                    </Discount>
                  </p>
                </React.Fragment>
              )
              : <Unavailable>Produto Indisponível</Unavailable>
          }
          <Button
            darkButton={false}
            onClick={goToProductPage}
          >
            ver detalhes
          </Button>
        </Infos>
      </div>
    )
    : <React.Fragment />
}

export default ShelfItem;
