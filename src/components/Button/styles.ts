import styled, { css } from 'styled-components';

import { colors, transitionTimingFunction } from '../../styles/variables';

const { primary, secondary, grey } = colors;

interface ContainerProps {
  darkButton: boolean;
}

export const Container = styled.button<ContainerProps>`
  margin-top: 10px 0;
  padding: 5px;
  text-transform: uppercase;
  border: 1px solid ${primary};
  transition: background-color ${transitionTimingFunction}, color ${transitionTimingFunction};

  &:hover {
    color: ${secondary};
    background-color: ${primary};
  }

  ${(props) => (props.darkButton && css `
    color: ${secondary};
    background-color: ${primary};
  `)}

  &[disabled] {
    color: ${secondary};
    background-color: ${grey};
    border-color: ${grey};
    cursor: not-allowed;
  }
`;
