import styled, { keyframes, css } from 'styled-components';

import { colors } from '../../styles/variables';

interface ContainerWrapper {
  itemsPerLine: number;
  itemHeightPx: number;
}

const { grey, greyLight } = colors;

const loading = keyframes`
  0% {
    background-color: ${grey};
  }

  50% {
    background-color: ${greyLight};
  }

  100% {
    background-color: ${grey};
  }
`;

const loadingReverse = keyframes`
  0% {
    background-color: ${greyLight};
  }

  50% {
    background-color: ${grey};
  }

  100% {
    background-color: ${greyLight};
  }
`;

export const Container = styled.div<ContainerWrapper>`
  display: flex;
  width: 100%;
  flex-wrap: wrap;

  > div {
    ${(props) => (
      css`
        width: ${Math.round(100 / props.itemsPerLine)}%;
        height: ${props.itemHeightPx}px;
      `
    )};


    &:nth-child(odd) {
      animation: 1500ms ${loading} ease-in both infinite;
    }

    &:nth-child(even) {
      animation: 1500ms ${loadingReverse} ease-in both infinite;
    }

  }
`;
