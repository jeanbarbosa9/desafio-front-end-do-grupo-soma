import React, { useState, useEffect } from 'react';

import { Container } from './styles';

interface LoadingPlaceholderProps {
  itemsPerLine: number;
  itemHeightPx: number;
  placeholderCount: number;
}

const LoadingPlaceholder: React.FC<LoadingPlaceholderProps> = (
  { itemsPerLine, placeholderCount, itemHeightPx }
) => {
  const [data, setData] = useState<string[]>([]);

  useEffect(() => {
    for(let index = 0; index < placeholderCount; index++) {
      setData((state) => [...state, 'loading']);
    }
  }, [placeholderCount])

  return (
    <Container itemsPerLine={itemsPerLine} itemHeightPx={itemHeightPx}>
      {data.map((item, index) => <div key={index} />)}
    </Container>
  )
}

export default LoadingPlaceholder;
