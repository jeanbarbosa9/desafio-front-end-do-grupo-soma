import styled,  { keyframes, css } from 'styled-components';

import {
  colors,
  transitionTimingFunction,
  breakpoints
} from '../../styles/variables';

interface FilterButtonProps {
  isOpen: boolean;
}

interface OptionsWrapperProps {
  isOpen: boolean;
}

const { primary, secondary, grey, greyDark, darkTransparence } = colors;
const { tablet_lg, desk_xs } = breakpoints;

const hoverFilterButton = keyframes`
  from {
    width: 0;
  }

  to {
    width: 100%;
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const FilterButton = styled.span<FilterButtonProps>`
  position: relative;
  display: flex;
  padding: 12px 0;
  align-items: center;
  font-size: 12px;
  text-transform: uppercase;
  cursor: pointer;

  svg {
    margin-left: 7px;
    transition: transform ${transitionTimingFunction};
  }

  + span {
    margin-left: 60px;
  }

  ${ (props) => (
    props.isOpen ? css`
      border-bottom: 1px solid ${primary};

      svg {
        transform: rotateX(180deg);
      }
      `
      : css`
        &:after {
          content: '';
          position: absolute;
          display: block;
          top: 100%;
          left: 0;
          height: 1px;
          background-color: ${primary};
        }

        &:hover {
          &:after {
            animation: 300ms ${hoverFilterButton} ease-in-out both;
          }
        }
      `
  )}
`;

export const FilterOverlay = styled.div<OptionsWrapperProps>`
  position: absolute;
  top: 100%;
  left: 0;
  width: 100%;
  height: calc(100vh - 40px);
  background: ${darkTransparence};
  opacity: 0;
  visibility: hidden;
  transition: opacity ${transitionTimingFunction};
  z-index: 1;

  ${(props) => (
    props.isOpen && css`
      opacity: 1;
      visibility: visible;
    `)
  }
`;

export const OptionsWrapper = styled.div<OptionsWrapperProps>`
  position: absolute;
  top: 100%;
  left: 0%;
  display: flex;
  width: 100%;
  padding: 20px;
  background: ${secondary};
  opacity: 0;
  visibility: hidden;
  transition: opacity ${transitionTimingFunction};
  z-index: 4;

  @media only screen and (max-width: ${tablet_lg}px) {
    flex-direction: column;
  }

  @media only screen and (min-width: ${desk_xs}px) {
    display: grid;
    grid-template-areas:
      'categories sizes'
      'selected selected';
    box-shadow: 0 3px 5px ${darkTransparence};

    z-index: 2;
  }

  ${(props) => (
    props.isOpen && css`
      opacity: 1;
      visibility: visible;
    `)
  }

  &[data-use="order-by"] {
    display: flex;
    justify-content: center;

    span {
      font-size: 14px;
      color: ${greyDark()};
      cursor: pointer;

      @media only screen and (max-width: ${tablet_lg}px) {
        padding: 10px 0;
      }

      &:hover,
      &[data-is-selected="true"] {
        color: ${primary};
        font-weight: 700;
      }

      + span {

        @media only screen and (min-width: ${desk_xs}px) {
          margin-left: 20px;
        }
      }
    }
  }
`;

export const FilterCategoriesWrapper = styled.div`
  display: flex;
  margin-bottom: 20px;
  flex-direction: column;
  align-items: flex-start;

  @media only screen and (min-width: ${desk_xs}px) {
    min-height: 120px;
    margin-bottom: 0;
    padding-right: 60px;
    grid-area: categories;
    align-items: flex-end;
    border-right: 1px solid ${grey};
  }

  p {
    margin-bottom: 10px;
    justify-self: flex-start;
    font-size: 12px;
    font-weight: 500;
    text-transform: uppercase;
  }

  > div {
    position: relative;
    display: grid;
    margin-top: 20px;
    grid-template-columns: repeat(2, 1fr);

    &:before {
      content: 'CATEGORIAS';
      position: absolute;
      bottom: calc(100% + 10px);
      left: 0;
      font-size: 12px;
    }

    span {
      display: block;
      padding: 10px;
      padding-left: 0;
      font-size: 11px;
      color: ${greyDark()};
      font-weight: 300;
      transition: color ${transitionTimingFunction}, font-weight ${transitionTimingFunction};
      cursor: pointer;

      &:nth-child(even) {
        padding-right: 0;
        padding-left: 10px;
      }

      &:hover,
      &[data-is-selected='true'] {
        color: ${primary};
        font-weight: 700;
      }
    }
  }
`;

export const FilterSizeWrapper = styled.div`
  display: flex;
  max-height: 120px;
  grid-area: sizes;

  @media only screen and (min-width: ${desk_xs}px) {
    padding-left: 60px;
  }

  > div {
    position: relative;
    display: grid;
    margin-top: 20px;
    grid-template-columns: repeat(5, 1fr);

    &:before {
      content: 'TAMANHOS';
      position: absolute;
      bottom: calc(100% + 10px);
      left: 0;
      font-size: 12px;
    }

    span {
      display: block;
      width: 35px;
      height: 35px;
      margin-right: 15px;
      margin-bottom: 15px;
      padding: 10px;
      font-size: 11px;
      color: ${greyDark()};
      text-align: center;
      font-weight: 300;
      border: 1px solid ${primary};
      transition: color ${transitionTimingFunction}, background-color ${transitionTimingFunction};
      cursor: pointer;

      &:hover,
      &[data-is-selected='true'] {
        color: ${secondary};
        background: ${primary};
      }
    }
  }
`;

export const SelectedFilters = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  grid-area: selected;
  justify-content: center;
  align-items: center;

  span {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    padding-bottom: 0;
    font-size: 11px;
    cursor: pointer;

    svg {
      margin-left: 5px;
    }
  }
`;

export const ClearFilters = styled.span`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
