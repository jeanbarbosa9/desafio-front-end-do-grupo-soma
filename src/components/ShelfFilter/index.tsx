import React, { useState, useCallback } from 'react';
import { FiChevronDown, FiX } from 'react-icons/fi';

import { useProducts } from '../../hooks/productsList';

import { colors } from '../../styles/variables';

import {
  Container,
  FilterButton,
  OptionsWrapper,
  FilterOverlay,
  FilterCategoriesWrapper,
  SelectedFilters,
  FilterSizeWrapper,
  ClearFilters,
} from './styles';

import Product from '../../interfaces/IProduct'

interface ShelfFilterProps {
  products: Product[];
}

interface orderByTextReferenceProps {
  [key: string]: string
}

interface filterResultLabelProps {
  [key: number]: string;
}

const ShelfFilter: React.FC<ShelfFilterProps> = ({ products }) => {
  const {
    clearFilters,
    filterByCategory,
    filterBySize,
    reapplyFilterByCategory,
    reapplyFilterBySize,
    filterBySizeAndCategory,
    orderByName,
    orderByPrice,
    orderByDiscount,
  } = useProducts();

  const [openFilter, setOpenFilter] = useState(false);
  const [openOrderBy, setOpenOrderBy] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState('');
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedOrderBy, setSelectedOrderBy] = useState('');
  const [orderByText, setOrderByText] = useState('Ordenar Por');

  const uniqueSizes: string[] = [];
  const uniqueCategoriesNames: string[] = []
  const orderByTextReference: orderByTextReferenceProps = {
    'name_ASC': 'Nome (A - Z)',
    'name_DESC': 'Nome (Z - A)',
    'price_ASC': 'Menor preço',
    'price_DESC': 'Maior preço',
    'discount_ASC': 'Menor desconto',
    'discount_DESC': 'Maior desconto',
  };
  const filterResultLabel: filterResultLabelProps = {
    0: 'Nenhum produto encontrado',
    1: 'Produto encontrado',
  }

  const productsCategoriesNames = products.map(product => {
    const { categoryId, categoriesIds, categories } = product;
    const categoryIndex = categoriesIds && categoriesIds.findIndex(
      category => categoryId && category.includes(categoryId)
    );
    const matchedCategories = (categories && categoryIndex !== undefined) && categories[categoryIndex];
    const matchedCategoriesArr = matchedCategories && matchedCategories.split('/');
    const categoryName = matchedCategoriesArr && matchedCategoriesArr[matchedCategoriesArr.length - 2];

    return categoryName
  })

  productsCategoriesNames.forEach(category => {
    if (category) {
      const findCategory = uniqueCategoriesNames.find(
        item => category === item
      );

      if (!findCategory) {
        uniqueCategoriesNames.push(category);
      }
    }
  })

  const productsItemsSizes = products.flatMap(product =>
    product.items?.map(item => (item && item.Tamanho) && item.Tamanho[0])
  );

  productsItemsSizes.forEach(size => {
    if (size) {
      const foundSize = uniqueSizes.find(currentSize => currentSize === size);

      if (!foundSize) {
        uniqueSizes.push(size)
      }
    }
  })

  uniqueCategoriesNames.sort();
  uniqueSizes.sort();

  const handleClearFilter = useCallback(() => {
    clearFilters();
    setSelectedCategory('');
    setSelectedSize('');
  }, [clearFilters])

  const handleFilterCategory = useCallback((category) => {

    if (category !== selectedCategory) {
      if (selectedSize) {
        filterBySizeAndCategory(selectedSize, category)
      } else {
        filterByCategory(category);
      }

      setSelectedCategory(category);
    } else {
      clearFilters();
      setSelectedCategory('');
      !!selectedSize && reapplyFilterBySize(selectedSize);
    }
  }, [
      selectedCategory,
      filterByCategory,
      clearFilters,
      selectedSize,
      reapplyFilterBySize,
      filterBySizeAndCategory
    ])

  const handleFilterSize = useCallback((size) => {
    if (size !== selectedSize) {
      if (selectedCategory) {
        filterBySizeAndCategory(size, selectedCategory);
      } else {
        filterBySize(size);
      }
      setSelectedSize(size);
    } else {
      clearFilters();
      setSelectedSize('');
      !!selectedCategory && reapplyFilterByCategory(selectedCategory);
    }
  }, [
      selectedSize,
      filterBySize,
      selectedCategory,
      clearFilters,
      reapplyFilterByCategory,
      filterBySizeAndCategory
    ])

  const handleOpenFilter = useCallback(() => {
    if (uniqueCategoriesNames.length || uniqueSizes.length) {
      setOpenFilter(!openFilter);

      if (openOrderBy) {
        setOpenOrderBy(false);
      }
    }
  }, [openFilter, openOrderBy, uniqueCategoriesNames, uniqueSizes])

  const handleOpenOrderBy = useCallback(() => {
    setOpenOrderBy(!openOrderBy);

    if (openFilter) {
      setOpenFilter(false);
    }
  }, [openFilter, openOrderBy])

  const closeFilterOrOrderBy = useCallback(() => {
    setOpenFilter(false);
    setOpenOrderBy(false);
  }, [])

  const handleOrderBy = useCallback((type: string, direction: string) => {
    const orderByAlias = `${type}_${direction}`;

    if (type === 'name') {
      orderByName(direction);
    };

    if (type === 'price') {
      orderByPrice(direction);
    }

    if (type === 'discount') {
      orderByDiscount(direction);
    }

    if (selectedCategory && selectedSize) {
      filterBySizeAndCategory(selectedSize, selectedCategory)
    }

    if (selectedCategory && !selectedSize) {
      filterByCategory(selectedCategory);
    }

    if (!selectedCategory && selectedSize) {
      filterBySize(selectedSize)
    }

    if (selectedOrderBy !== orderByAlias) {
      setSelectedOrderBy(orderByAlias);
      setOrderByText(orderByTextReference[orderByAlias]);
      setOpenOrderBy(false);
    } else {
      window.location.reload()
    }

  }, [
      orderByName,
      selectedOrderBy,
      selectedCategory,
      selectedSize,
      filterBySizeAndCategory,
      filterByCategory,
      filterBySize,
      orderByDiscount,
      orderByPrice,
      orderByTextReference,
    ])

  return (
    <Container>
      <FilterButton onClick={handleOpenFilter} isOpen={openFilter}>
        Filtrar por <FiChevronDown size={14} color={colors.primary} />
      </FilterButton>
      <FilterButton onClick={handleOpenOrderBy} isOpen={openOrderBy}>
        {orderByText} <FiChevronDown size={14} color={colors.primary} />
      </FilterButton>
      <FilterOverlay
        onClick={closeFilterOrOrderBy}
        isOpen={openFilter || openOrderBy}
      />
      <OptionsWrapper isOpen={openFilter}>
        <FilterCategoriesWrapper>
          <div>
            {
              uniqueCategoriesNames.map(category => {
                const categoryFiltered = selectedCategory === category;

                return (
                  <span
                    key={category}
                    onClick={() => handleFilterCategory(category)}
                    data-is-selected={categoryFiltered}
                  >
                    {category}
                  </span>
                )
              })
            }
          </div>
        </FilterCategoriesWrapper>
        <FilterSizeWrapper>
          <div>
            {
              uniqueSizes.map(size => {
                const sizeIsFiltered = selectedSize === size;

                return (
                  <span
                    key={size}
                    onClick={() => {handleFilterSize(size)}}
                    data-is-selected={sizeIsFiltered}
                  >
                    {size}
                  </span>
                )
              })
            }
          </div>
        </FilterSizeWrapper>
        <SelectedFilters>
          {
            products.length <= 1
              ? (
                  <span>
                    {products.length} {filterResultLabel[products.length]}
                  </span>
                )
              : (
                  <span>
                    {products.length} produtos encontrados
                  </span>
              )
          }
          {
            !!selectedCategory || !!selectedSize
              ? (
                <ClearFilters onClick={handleClearFilter}>
                  Limpar Filtros <FiX size={14} color={colors.primary} />
                </ClearFilters>
              )
              : ''
          }
        </SelectedFilters>
      </OptionsWrapper>
      <OptionsWrapper data-use="order-by" isOpen={openOrderBy}>
        <span
          data-is-selected={selectedOrderBy === 'price_ASC'}
          onClick={() => handleOrderBy('price', 'ASC')}
        >
          {orderByTextReference['price_ASC']}
        </span>
        <span
          data-is-selected={selectedOrderBy === 'discount_DESC'}
          onClick={() => handleOrderBy('discount', 'DESC')}>
            {orderByTextReference['discount_DESC']}
        </span>
        <span
          data-is-selected={selectedOrderBy === 'name_ASC'}
          onClick={() => handleOrderBy('name', 'ASC')}
        >
          {orderByTextReference['name_ASC']}
        </span>
        <span
          data-is-selected={selectedOrderBy === 'name_DESC'}
          onClick={() => handleOrderBy('name', 'DESC')}>
            {orderByTextReference['name_DESC']}
        </span>
        <span
          data-is-selected={selectedOrderBy === 'price_DESC'}
          onClick={() => handleOrderBy('price', 'DESC')}>
            {orderByTextReference['price_DESC']}
        </span>
        <span
          data-is-selected={selectedOrderBy === 'discount_ASC'}
          onClick={() => handleOrderBy('discount', 'ASC')}
        >
          {orderByTextReference['discount_ASC']}
        </span>

      </OptionsWrapper>
    </Container>
  )
}

export default ShelfFilter;
