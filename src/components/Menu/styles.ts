import styled, { keyframes } from 'styled-components';

import { colors, breakpoints, fadeIn } from '../../styles/variables';

const { primary, greyDark, secondary } = colors;
const { tablet_lg } = breakpoints;

const hoverLink = keyframes`
  from {
    width: 0;
  }

  to {
    width: 100%;
  }
`;

export const NavContainer = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;

  @media only screen and (max-width: ${tablet_lg}px) {
    position: fixed;
    top: 56px;
    left: 0;
    flex-direction: column;
    width: 100%;
    height: 100%;
    animation: 500ms ${fadeIn} ease-in-out both;
    justify-content: flex-start;
    align-items: flex-start;
    background-color: ${secondary};
    z-index: 4;
  }

  a {
    position: relative;
    margin: 0 21px;
    padding-bottom: 27px;
    font-size: 12px;
    color: ${greyDark()};
    text-transform: uppercase;
    text-decoration: none;
    border-bottom-width: 2px;

    @media only screen and (max-width: ${tablet_lg}px) {
      margin-bottom: 17px;
      padding-bottom: 10px;
    }

    &:after {
      content: '';
      position: absolute;
      top: 100%;
      left: 0;
      height: 1px;
      background-color: ${primary};
    }

    &:hover {
      color: ${primary};

      &:after {
        animation: 300ms ${hoverLink} ease-in-out both;
      }
    }

    img {
      max-width: 35px;
    }
  }
`;
