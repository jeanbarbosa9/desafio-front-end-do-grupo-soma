import React, { useCallback } from 'react';
import { NavLink } from 'react-router-dom';

import { useDevice } from '../../hooks/device';

import insideText from '../../assets/menu_inside_text.png';

import { colors } from '../../styles/variables'

import { NavContainer } from './styles';

interface MenuProps {
  callback?: Function;
}

const Menu: React.FC<MenuProps> = ({ callback }) => {
  const { isMobile } = useDevice();

  const active = {
    color: colors.primary,
    fontWeight: 700,
    borderBottom: `2px solid ${colors.primary}`
  }

  const items = [
    {
      link: '/novidades',
      text: 'novidades',
      customFont: '',
    },
    {
      link: '/colecao',
      text: 'coleção',
      customFont: '',
    },
    {
      link: '/joias',
      text: 'joias',
      customFont: ''
    },
    {
      link: '/sale',
      text: 'sale',
      customFont: '',
    },
    {
      link: '/inside',
      text: 'inside',
      customFont: insideText,
    },
  ]

  const handleShowMenu = useCallback(
    () => {
      (isMobile && callback) && callback()
    },
    [isMobile, callback],
  )

  return (
      <NavContainer>
        {
          items.map(item =>  (
              <NavLink
                key={item.link}
                to={item.link}
                activeStyle={active}
                onClick={handleShowMenu}
              >
                {
                  !!item.customFont
                    ? (
                        <img
                          src={item.customFont}
                          alt={`Fonte customizada para ${item.text}`}
                        />
                      )
                    : item.text
                }
              </NavLink>
            )
          )
        }
      </NavContainer>
  )
}

export default Menu;
