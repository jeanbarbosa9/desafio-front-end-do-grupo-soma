import styled from 'styled-components';

import { colors, breakpoints } from '../../styles/variables';

const { primary } = colors;
const { tablet_lg, desk_xs, desk_lg } = breakpoints;

export const Container = styled.div`
  a, span {
    font-size: 11px;
    color: ${primary};
    text-transform: capitalize;
    text-decoration: none;

    @media only screen and (max-width: ${tablet_lg}px) {
      text-transform: uppercase;
    }

    @media only screen and (min-width: ${desk_lg}px) {
      font-size: 12px;
    }
  }


  span {
    position: relative;

    @media only screen and (max-width: ${tablet_lg}px) {
      padding-left: 10px;
    }

    @media only screen and (min-width: ${desk_xs}px) {
      margin-left: 20px;

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: -12px;
        display: inline-block;
        border-width: 3px 0 3px 5px;
        border-style: solid;
        border-color: transparent ${primary};
        line-height: 14px;
        transform: translateY(-50%);
      }
    }
  }
`;
