import React from 'react';
import { Link } from 'react-router-dom';

import { useDevice } from '../../hooks/device';

import { Container } from './styles';

export interface PathProps {
  link: string;
  text: string;
}

interface BreadCrumbProps {
  paths: PathProps[];
}

const BreadCrumb: React.FC<BreadCrumbProps> = ({ paths }) => {
  const { isMobile } = useDevice();

  return (
    <Container>
      {
        isMobile
        ? ( <span>{paths[paths.length - 1].text}</span> )
        : paths.map((path, index, arr) => {
            if (index < arr.length - 1) {
              return (<Link key={index} to={path.link}>{path.text}</Link>)
            } else {
              return (<span key={index}>{path.text}</span>)
            }
          })
      }
    </Container>
  )
}


export default BreadCrumb;
