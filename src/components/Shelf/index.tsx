import React, { useState, useRef, MouseEvent, useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import { useLoading } from '../../hooks/loading';
import { useDevice } from '../../hooks/device';

import ProductProps from '../../interfaces/IProduct';
import pathsDictionary from '../../utils/pathsDictionary';

import LoadingPlaceholder from '../LoadingPlaceholder';
import BreadCrumb from '../BreadCrumb';
import ShelfFilter from '../ShelfFilter';
import ShelfItem from '../ShelfItem';
import {
  Container,
  ActionsWrapper,
  ShelfItemWrapper,
  ShelfVisualization,
} from './styles';

interface ShelfProps {
  products: ProductProps[];
}

const Shelf: React.FC<ShelfProps> = ({ products }) => {
  const { pathname } = useLocation();
  const { isLoading } = useLoading();
  const { isMobile } = useDevice();

  const [productsPerLine, setProductsPerLine] = useState(2);
  const buttonRef1 = useRef<HTMLButtonElement>(null);
  const buttonRef2 = useRef<HTMLButtonElement>(null);

  const path = pathname.split('/')[1]
  const paths = [
    {
      text: 'home',
      link: '/',
    },
    {
      text: pathsDictionary[path],
      link: path,
    },
  ]

  useEffect(() => {
    setProductsPerLine(isMobile ? 2 : 4);
  }, [isMobile])

  const handleShelfVisualization = (event: MouseEvent): void => {
    if (buttonRef1.current && buttonRef2.current) {
      buttonRef1.current.dataset.active = 'false';
      buttonRef2.current.dataset.active = 'false';

      if (event.currentTarget) {
        event.currentTarget.setAttribute('data-active', 'true');
      }
    }

    const changeProductsPerPage = parseInt(event.currentTarget.innerHTML);

    if (typeof changeProductsPerPage === 'number') {
      setProductsPerLine(changeProductsPerPage);
    } else {
      console.error('Por favor insira um número como texto do botão');
    }
  }

  return (
    <Container>
      { isMobile && <BreadCrumb paths={paths} /> }
      <ActionsWrapper>
        { !isMobile && <BreadCrumb paths={paths} /> }
        <ShelfFilter products={products} />
        {
          !isMobile
            ? (
              <ShelfVisualization>
                <span>Visualizar:</span>
                <button
                  ref={buttonRef1}
                  onClick={handleShelfVisualization}
                  data-active={productsPerLine === 2}
                >2</button>
                <button
                  ref={buttonRef2}
                  onClick={handleShelfVisualization}
                  data-active={productsPerLine === 4}
                >4</button>
              </ShelfVisualization>
            )
            : <React.Fragment />
        }

      </ActionsWrapper>
      {
        isLoading && !products.length
          ? (
              <LoadingPlaceholder
                itemsPerLine={productsPerLine}
                itemHeightPx={505}
                placeholderCount={productsPerLine}/>
          )
          : (
            <ShelfItemWrapper productsPerLine={productsPerLine}>
              {products.map((product: ProductProps)  => (
                <ShelfItem
                  key={product.productId}
                  sizeInShelf={productsPerLine}
                  productId={product.productId}
                  productName={product.productName}
                  productReference={product.productReference}
                  linkText={product.linkText}
                  items={product.items}
                />
              ))}
            </ShelfItemWrapper>
          )
      }
      {
        isLoading && products.length
          ? (
              <LoadingPlaceholder
                itemsPerLine={productsPerLine}
                itemHeightPx={505}
                placeholderCount={productsPerLine}
              />
          )
          : ''
      }
    </Container>
  )
}

export default Shelf;
