import styled, { css } from 'styled-components';
import { colors, transitionTimingFunction, breakpoints } from '../../styles/variables';

const { primary, lightTransparence } = colors;
const { desk_xs } = breakpoints;

interface ShelfItemWrapperProps {
  productsPerLine: number;
}

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const ActionsWrapper = styled.div`
  position: sticky;
  top: 0;
  left: 0;
  display: flex;
  width: 100%;
  padding: 0 50px;
  justify-content: center;
  align-items: center;
  background: ${lightTransparence};
  z-index: 1;

  @media only screen and (min-width: ${desk_xs}px) {
    justify-content: space-between;
  }
`;

export const BreadCrumbs = styled.div`
  span {
    font-size: 12px;
    text-transform: capitalize;
  }

  span + span {
    position: relative;
    margin-left: 20px;

    &:before {
      content: '';
      position: absolute;
      top: 50%;
      left: -12px;
      display: inline-block;
      border-width: 3px 0 3px 5px;
      border-style: solid;
      border-color: transparent ${primary};
      line-height: 14px;
      transform: translateY(-50%);
    }
  }
`;

export const ShelfItemWrapper = styled.div<ShelfItemWrapperProps>`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;

  > div {
    transition: max-width ${transitionTimingFunction};

    ${(props) => (
      css`
        max-width: ${Math.round(100 / props.productsPerLine)}%;
      `
    )};
  }
`;

export const ShelfVisualization = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    margin-right: 5px;
    font-size: 12px;
  }

  button {
    padding: 10px 12px;
    font-size: 16px;

    &[data-active='true'] {
      font-weight: bold;
      border-bottom: 2px solid ${primary};
    }
  }
`;
