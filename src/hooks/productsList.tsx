import React, {
  createContext,
  useState,
  useCallback,
  useContext,
} from 'react';

import Product from '../interfaces/IProduct';

interface ProductsListContextData {
  products: Product[];
  setProducts: Function;
  allProducts: Product[];
  setAllProducts: Function;
  hasFilter: boolean;
  filteredProducts: Product[];
  findProduct: Function;
  getProductCategory: Function;
  clearFilters: Function;
  filterByCategory: Function;
  reapplyFilterByCategory: Function;
  filterBySize: Function;
  reapplyFilterBySize: Function;
  filterBySizeAndCategory: Function;
  orderByName: Function;
  orderByPrice: Function;
  orderByDiscount: Function;
}

const ProductsListContext = createContext<ProductsListContextData>({} as ProductsListContextData);

const ProductsListProvider:React.FC = ({ children }) => {
  const [hasFilter, setHasFilter] = useState(false);
  const [allProductsData, setAllProductsData] = useState<Product[]>([]);
  const [productsData, setProductsData] = useState<Product[]>([]);
  const [filteredProducts, setFilteredProducts] = useState<Product[]>([]);

  const setProducts = useCallback((products) => {
    setProductsData(products);
  }, [setProductsData])

  const setAllProducts = useCallback((products) => {
    setAllProductsData(products);
  }, [setAllProductsData])

  const findProduct = useCallback((link: string) => {
    const foundProduct = allProductsData.find(product => product.linkText === link);
    return foundProduct;
  }, [allProductsData])

  const getProductCategory = useCallback((link: string) => {
    const productCategory = (product: Product) => {
      const { categoryId, categories, categoriesIds } = product;
      const categoryIndex = categoriesIds && categoriesIds.findIndex(
        item => categoryId && item.includes(categoryId)
      );
      const matchedCategoriesArray = categories && categoryIndex !== undefined
        && categories[categoryIndex].split('/');
      const category = matchedCategoriesArray && matchedCategoriesArray[matchedCategoriesArray.length - 2];

      return category && category.toLowerCase();
    }

    const foundProduct = findProduct(link);
    return foundProduct && productCategory(foundProduct);
  }, [findProduct])

  const clearFilters = useCallback(() => {
    setFilteredProducts([]);
    setHasFilter(false);
  }, [])

  const filterByCategory = useCallback(async (category: string) => {
    const productsBase = hasFilter ? filteredProducts : productsData;

    const filtered = productsBase.filter(product =>
      product.categories && product.categories[0].split('/').find(
        item => item.includes(category),
      ),
    );

    setFilteredProducts([]);
    setFilteredProducts(filtered);
    setHasFilter(true);
  }, [productsData, hasFilter, filteredProducts])

  const reapplyFilterByCategory = useCallback(async (category: string) => {
    const filtered = productsData.filter(product =>
      product.categories && product.categories[0].split('/').find(
        item => item.includes(category),
      ),
    );

    setFilteredProducts(filtered);
    setHasFilter(true);
  }, [productsData])

  const filterBySize = useCallback((size: string) => {
    const productsBase = hasFilter ? filteredProducts : productsData;

    const filtered = productsBase.filter(product =>
      product.items && product.items.find(
        item => {
          return item.Tamanho?.includes(size)
        }
      ),
    );

    setFilteredProducts(filtered);
    setHasFilter(true);
  }, [hasFilter, productsData, filteredProducts]);

  const reapplyFilterBySize = useCallback((size: string) => {
    const filtered = productsData.filter(product =>
      product.items && product.items.find(
        item => {
          return item.Tamanho?.includes(size)
        }
      ),
    );

    setFilteredProducts(filtered);
    setHasFilter(true);
  }, [productsData])

  const filterBySizeAndCategory = useCallback(
    (size: string, category: string) => {
      const filteredCategory = productsData.filter(product =>
        product.categories && product.categories[0].split('/').find(
          item => item.includes(category),
        ),
      );

      const filtered = filteredCategory.filter(product =>
        product.items && product.items.find(
          item => {
            return item.Tamanho?.includes(size)
          }
        ),
      );

    setFilteredProducts(filtered);
    setHasFilter(true);
  }, [productsData])

  const orderByName = useCallback((direction: 'ASC' | 'DESC') => {
    const productsBase = filteredProducts.length
      ? filteredProducts
      : productsData;

    const ordered = productsBase.sort(
      (productA, productB) => {
        const compare = direction === 'ASC'
          ? productA.productName < productB.productName
          : productA.productName > productB.productName

        if (compare) {
          return -1
        } else {
          return 1
        }
      }
    )

    !hasFilter && clearFilters();
    setTimeout(() => {
      setFilteredProducts(ordered);
    }, 10)
  }, [clearFilters, hasFilter, filteredProducts, productsData])

  const orderByPrice = useCallback((direction: 'ASC' | 'DESC') => {
    const productsBase = filteredProducts.length
      ? filteredProducts
      : productsData;

    const ordered = productsBase.sort(
      (productA, productB) => {
        const conditionA = (productA.items && productA.items[0].sellers)
          && productA.items[0].sellers[0].commertialOffer.Price;
        const conditionB = (productB.items && productB.items[0].sellers)
          && productB.items[0].sellers[0].commertialOffer.Price;

        if (conditionA && conditionB) {
          const compare = direction === 'ASC'
            ? conditionA < conditionB
            : conditionA > conditionB

          if (compare) {
            return -1
          } else {
            return 1
          }
        } else {
          return 0;
        }
      }
    )

    !hasFilter && clearFilters();
    setTimeout(() => {
      setFilteredProducts(ordered);
    }, 10)
  }, [clearFilters, hasFilter, filteredProducts, productsData])

  const orderByDiscount = useCallback((direction: 'ASC' | 'DESC') => {
    const productsBase = filteredProducts.length
      ? filteredProducts
      : productsData;

    const ordered = productsBase.sort(
      (productA, productB) => {
        const conditionA = (productA.items && productA.items[0].sellers)
          && productA.items[0].sellers[0].commertialOffer.Price / productA.items[0].sellers[0].commertialOffer.ListPrice;
        const conditionB = (productB.items && productB.items[0].sellers)
          && productB.items[0].sellers[0].commertialOffer.Price / productB.items[0].sellers[0].commertialOffer.ListPrice;

        if (conditionA && conditionB) {
          const compare = direction === 'ASC'
            ? conditionA < conditionB
            : conditionA > conditionB

          if (compare) {
            return -1
          } else {
            return 1
          }
        } else {
          return 0;
        }
      }
    )

    !hasFilter && clearFilters();
    setTimeout(() => {
      setFilteredProducts(ordered);
    }, 10)
  }, [clearFilters, hasFilter, filteredProducts, productsData])

  return (
    <ProductsListContext.Provider
      value={{
        products: productsData,
        setProducts,
        allProducts: allProductsData,
        setAllProducts,
        hasFilter,
        filteredProducts,
        findProduct,
        getProductCategory,
        clearFilters,
        filterByCategory,
        reapplyFilterByCategory,
        filterBySize,
        reapplyFilterBySize,
        filterBySizeAndCategory,
        orderByName,
        orderByPrice,
        orderByDiscount,
    }}
    >
      {children}
    </ProductsListContext.Provider>
  )
}

function useProducts(): ProductsListContextData {
  const context = useContext(ProductsListContext);

  if(!context) {
    throw new Error('useProducts must be used inside a ProductsListProvider');
  }

  return context;
}

export { ProductsListProvider, useProducts }
