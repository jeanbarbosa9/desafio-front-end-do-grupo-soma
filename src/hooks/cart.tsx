import React, { createContext, useState, useCallback, useContext } from 'react';

export interface CartProduct {
  id: string;
  name: string;
  imageUrl: string;
  quantity: number;
  price: number;
}

interface NotificationProps {
  title: string;
  product: CartProduct;
}

interface CartContextData {
  cartProducts: CartProduct[];
  notification: NotificationProps;
  addCartProduct: Function;
  removeCartProduct: Function;
}

const CartContext = createContext<CartContextData>({} as CartContextData);

const CartProvider: React.FC = ({children}) => {
  const [products, setProducts] = useState<CartProduct[]>([]);
  const [notification, setNotification] = useState({} as NotificationProps);

  const addCartProduct = useCallback(
    (product: CartProduct) => {
      const findProduct = products.find(
        foundProduct => foundProduct.id === product.id
      );

      if (!findProduct) {
        product.quantity = 1;
        setProducts((state) => [...state, product]);
        setNotification({
          title: 'Produto adicionado!',
          product,
        })
      } else {
        let changedProduct = {} as CartProduct;

        const productsChanged = products.map(product => {
          if (product.id === findProduct.id) {
            product.quantity++;
            changedProduct = product;
          }

          return product;
        })

        setProducts(productsChanged);
        setNotification({
          title: 'Quantidade adicionada!',
          product: changedProduct,
        })
      }

      setTimeout(() => setNotification({} as NotificationProps), 3000);
    },
    [products],
  )

  const removeCartProduct = useCallback(
    (product: CartProduct) => {
      const findProduct = products.find(foundProduct => foundProduct.id === product.id);

      if (findProduct) {
        if (findProduct?.quantity > 1) {
          const productsChanged = products.map(product => {
            if (product.id === findProduct.id) {
              product.quantity--;
            }

            return product;
          })

          setProducts(productsChanged);
        } else {
          const productsChanged = products.filter(
            product => product.id !== findProduct.id
          );

          setProducts(productsChanged);
        }
      }
    },
    [products],
  )

  return (
    <CartContext.Provider value={{
      cartProducts: products,
      notification,
      addCartProduct,
      removeCartProduct,
    }}>
      {children}
    </CartContext.Provider>
  )
}

function useCart(): CartContextData {
  const context = useContext(CartContext);

  if(!context) {
    throw new Error('useCart must be used inside a CartProvider');
  }

  return context;
}

export { CartProvider, useCart };
