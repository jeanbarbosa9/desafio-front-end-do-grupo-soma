import React, { createContext, useState, useContext, useEffect } from 'react';

import { breakpoints } from '../styles/variables'

interface DeviceContextData {
  isMobile: boolean;
}

const DeviceContext = createContext<DeviceContextData>({} as DeviceContextData);

const DeviceProvider: React.FC = ({children}) => {
  const [isMobile, setIsMobile] = useState(true);

  useEffect(() => {
    window.outerWidth > breakpoints.tablet_lg && setIsMobile(false);

    window.addEventListener('resize', () => {
      setIsMobile(window.outerWidth < breakpoints.tablet_lg);
    })


  }, [])

  return (
    <DeviceContext.Provider value={{
      isMobile,
    }}>
      {children}
    </DeviceContext.Provider>
  )
}

function useDevice(): DeviceContextData {
  const context = useContext(DeviceContext);

  if(!context) {
    throw new Error('useDevice must be used inside a CartProvider');
  }

  return context;
}

export { DeviceProvider, useDevice };
