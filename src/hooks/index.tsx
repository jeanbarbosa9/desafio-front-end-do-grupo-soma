import React from 'react';

import { CartProvider } from './cart';
import { LoadingProvider } from './loading';
import { DeviceProvider } from './device';

const AppProvider: React.FC = ({ children }) => (
  <LoadingProvider>
    <DeviceProvider>
      <CartProvider>
        {children}
      </CartProvider>
    </DeviceProvider>
  </LoadingProvider>
);

export default AppProvider;
