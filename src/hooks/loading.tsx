import React, { createContext, useContext, useState, useCallback } from 'react';

interface LoadingContextData {
  isLoading: boolean;
  setIsLoading: Function;
};

const LoadingContext = createContext<LoadingContextData>({} as LoadingContextData)

const LoadingProvider: React.FC = ({ children }) => {
  const [data, setData] = useState(false);

  const setIsLoading = useCallback((state: boolean) => {
    setData(state);
  }, []);

  return (
    <LoadingContext.Provider value={{ isLoading: data, setIsLoading }}>
      { children }
    </LoadingContext.Provider>
  )
}

function useLoading(): LoadingContextData {
  const context = useContext(LoadingContext);

  if(!context) {
    throw new Error('useLoading must be used inside a LoadingProvider')
  }

  return context;
}

export { LoadingProvider, useLoading };
