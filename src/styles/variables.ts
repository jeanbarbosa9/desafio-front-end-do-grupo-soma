import { keyframes } from 'styled-components';
import { darken } from 'polished';

export const colors = {
  primary: '#000000',
  secondary: '#ffffff',
  darkTransparence: 'rgba(0, 0, 0, 0.5)',
  green: '#138533',
  grey: '#cccccc',
  greyDark: () => darken(0.5, colors.grey),
  greyLight: '#eeeeee',
  lightTransparence: 'rgba(255, 255, 255, 0.7)',
  red: '#A30000',
}

export const transitionTimingFunction = '300ms ease-in-out';

export const breakpoints = {
  mobile_sm: 320,
  mobile_lg: 767,
  tablet_sm: 768,
  tablet_lg: 1024,
  desk_xs: 1025,
  desk_sm: 1280,
  desk_md: 1366,
  desk_lg: 1920,
  desk_hg: 2560,
}

export const fadeIn = keyframes`
  from {
    opacity: 0;
    visibility: hidden;
  }

  to {
    opacity: 1;
    visibility: visible;
  }
`;
