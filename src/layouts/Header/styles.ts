import styled, { keyframes } from 'styled-components';

import { colors, breakpoints, fadeIn } from '../../styles/variables'

const { darkTransparence,  secondary, grey } = colors;
const { desk_xs, tablet_sm } = breakpoints;

const bagReceiveProduct = keyframes`
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(1.3);
  }
  100% {
    transform: scale(1);
  }
`;

export const LogoContainer = styled.div`
  display: flex;
  padding: 35px 0 14px 0;
  justify-content: center;
  align-items: center;

  img {
    max-width: 80px;
  }
`;

export const IconsMenuContainer = styled.div`
  display: flex;
  padding: 15px;
  justify-content: space-between;
  align-items: center;

  .menu-mobile {
    margin-right: 30px;
  }

  .logo-img {
    max-width: 80px;
  }

  @media only screen and (min-width: ${desk_xs}px) {
    padding: 14px 45px 0 45px;
  }
`;

export const IconsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    max-width: 15px;
    max-height: 15px;

     + img {
       margin-left: 22px;
     }
  }
`;

export const MiniCartContainer = styled.div`
  position: relative;
  display: flex;
  margin-right: 22px;
  align-items: center;

  @media only screen and (min-width: ${tablet_sm}px) {
    margin: 0 22px;
  }

  &[data-product-added='true'] {
    > img, > span {
      animation: 1s ${bagReceiveProduct} ease-in-out infinite;
    }
  }

  > span {
    position: absolute;
    top: 0;
    left: calc(100% + 2px);
    font-size: 9px;
    font-weight: 700;
  }
`;

export const NotificationBaloonWrapper = styled.div`
  position: absolute;
  top: 0;
  right: 250%;
  width: 200px;
  padding: 5px;
  animation: 500ms ${fadeIn} ease-in-out both;
  background: ${secondary};
  border-radius: 3px;
  box-shadow: 0 3px 7px ${darkTransparence};
  z-index: 4;

  @media only screen and (min-width: ${desk_xs}px) {
    top: 50%;
    transform: translateY(-50%);
  }

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 100%;
    border-width: 10px;
    border-style: solid;
    border-color: transparent transparent transparent ${grey};

    @media only screen and (min-width: ${desk_xs}px) {
      top: 50%;
      transform: translateY(-50%);
    }

  }

  > p {
    margin-bottom: 10px;
    font-weight: 700;
    text-align: center;
  }

  p {
    margin-bottom: 5px;
    font-size: 12px;
  }

  > div {
    display: flex;
    justify-content: space-between;

    img {
      max-width: 50px;
      max-height: 50px;
      margin-right: 10px;
      transform: scale(1);
    }
  }
`;
