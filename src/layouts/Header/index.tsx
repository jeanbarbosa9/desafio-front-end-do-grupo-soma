import React, { useState, useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { FiMenu, FiX } from 'react-icons/fi';

import { useCart } from '../../hooks/cart';
import { useDevice } from '../../hooks/device';
import { formatMoney } from '../../helpers/formatNumber'

import Menu from '../../components/Menu';

import { colors } from '../../styles/variables';

import {
  LogoContainer,
  IconsMenuContainer,
  IconsContainer,
  MiniCartContainer,
  NotificationBaloonWrapper,
} from './styles';

import logoSrc from '../../assets/logo.png';
import locationIcon from '../../assets/lojas.png';
import contactIcon from '../../assets/contato.png';
import loginIcon from '../../assets/login.png';
import bagIcon from '../../assets/sacola.png';
import searchIcon from '../../assets/busca.png';

const Header: React.FC = () => {
  const { cartProducts, notification } = useCart();
  const { isMobile } = useDevice();
  const [productInCart, setProductInCart] = useState(false);
  const [showMenu, setShowMenu] = useState(false);

  useEffect(() => {
    if (cartProducts.length) {
      setProductInCart(true);

      setTimeout(() => {
        setProductInCart(false);
      }, 3000)
    }
  }, [cartProducts])

  const toggleMenu = useCallback(
    () => {
      setShowMenu(!showMenu)
    },
    [showMenu],
  )

  const NotificationBaloon = () => {
    const { title, product } = notification;

    if (notification && product) {
      return (
        <NotificationBaloonWrapper>
          <p>{title}</p>
          <div>
            <img
              src={product.imageUrl}
              alt={`Imagem do produto ${product.name}`}
            />
            <div>
              <p>{product.name}</p>
              <p>
                <span>Preço: </span>
                <strong>{formatMoney(product.price)}</strong>
              </p>
              <p>
                <span>Quantidade: </span>
                <strong>{product.quantity}</strong>
              </p>
            </div>
          </div>
        </NotificationBaloonWrapper>
      )
    }

    return ( <React.Fragment /> )
  }

  const BagIconWithCount = () => (
    <MiniCartContainer data-product-added={productInCart}>
      <img src={bagIcon} alt='Ícone de sacola' />
      <span>{cartProducts.length > 0 ? cartProducts.length : '' }</span>
      <NotificationBaloon />
    </MiniCartContainer>
  )

  const SearchIcon = () => (
    <img src={searchIcon} alt='Ícone de busca' />
  )

  const HeaderSM = () => (
    <React.Fragment>
      <IconsMenuContainer>
        <button onClick={toggleMenu}>
          {
            showMenu
              ? (
                <FiX
                  className='menu-mobile'
                  size={22}
                  color={colors.primary}
                />
              )
              : (
                <FiMenu
                  className='menu-mobile'
                  size={20}
                  color={colors.primary}
                />
              )
          }
        </button>
        <Link to="/">
          <img className='logo-img' src={logoSrc} alt="Logo Animale"/>
        </Link>
        <IconsContainer>
          <BagIconWithCount />
          <SearchIcon />
        </IconsContainer>
      </IconsMenuContainer>
      {
        showMenu
          ? <Menu callback={toggleMenu} />
          : <React.Fragment />
      }
    </React.Fragment>
  );

  const HeaderLG = () => (
    <React.Fragment>
      <Link to="/">
        <LogoContainer>
          <img src={logoSrc} alt="Logo Animale"/>
        </LogoContainer>
      </Link>
      <IconsMenuContainer>
        <IconsContainer>
          <img src={locationIcon} alt='Ícone de localização' />
          <img src={contactIcon} alt='Ícone de contato' />
        </IconsContainer>
        <Menu />
        <IconsContainer>
          <img src={loginIcon} alt='Ícone de login' />
          <BagIconWithCount />
          <SearchIcon />
        </IconsContainer>
      </IconsMenuContainer>
    </React.Fragment>
  )

  const RenderHeader: React.FC = () => {
    if (isMobile) {
      return <HeaderSM />
    } else {
      return <HeaderLG />
    }
  }

  return <RenderHeader />
}

export default Header;
