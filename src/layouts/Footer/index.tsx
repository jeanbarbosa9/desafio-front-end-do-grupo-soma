import React, { useCallback } from 'react';
import { FiChevronDown } from 'react-icons/fi';

import { colors } from '../../styles/variables';

import {
  NewsLetterContainer,
  NewsLetterTextFormContainer,
  NewsLetterTextContainer,
  NewsLetterForm,
  LinksContainer,
  LinksList,
  CorporateInfos,
} from './styles';

const Footer: React.FC = () => {
  const links = [
    'a marca',
    'minha conta',
    'políticas',
    'formas de pagamento',
    'newsletter'
  ];

  const handleSubmitForm = useCallback(
    (event) => {
      event.preventDefault();
    },
    [],
  )

  return (
    <React.Fragment>
      <NewsLetterContainer>
        <NewsLetterTextFormContainer>
          <NewsLetterTextContainer>
            <h3>Newsletter</h3>
            <p>Cadastre-se para receber nossas atividades e promoções:</p>
          </NewsLetterTextContainer>
          <NewsLetterForm>
            <input placeholder="nome" name="name" />
            <div>
              <input placeholder="e-mail" name="email" />
              <button onClick={handleSubmitForm}>ok</button>
            </div>
          </NewsLetterForm>
        </NewsLetterTextFormContainer>
      </NewsLetterContainer>
      <LinksContainer>
        <LinksList>
          <li className='icon'>
            <FiChevronDown size={17} color={colors.secondary} />
          </li>
          {
            links.map((link, index) => (
              <li key={index}>{link}</li>
            ))
          }
        </LinksList>
      </LinksContainer>
      <CorporateInfos>
        <p>RBX Rio Comércio de Roupas LTDA. Estrada dos Bandeirantes, 1700 - Galpão 03, Armazén 104 - Taquara, Rio de Janeiro, RJ - CEP: 22775-109. CNPJ: 10.285.590/0002-80</p>
      </CorporateInfos>
    </React.Fragment>
  )
}

export default Footer;
