import styled from 'styled-components';

import { colors } from '../../styles/variables';
import { breakpoints } from '../../styles/variables';

const { primary, secondary, grey, greyDark } = colors;
const { mobile_lg, tablet_lg, desk_xs, desk_lg } = breakpoints;

export const NewsLetterContainer = styled.div`
  display: flex;
  justify-content: space-between;
  background-color: ${grey};

  @media only screen and (min-width: ${desk_xs}px) {
    display: grid;
    padding: 30px;
    grid-template-areas: '. . . newscontent';
  }

`;

export const NewsLetterTextFormContainer = styled.div`
  width: 100%;
  padding: 20px;

  @media only screen and (min-width: ${desk_xs}px) {
    display: grid;
    grid-area: newscontent;
    grid-template-columns: repeat(2, 1fr);
  }

  @media only screen and (min-width: ${desk_lg}px) {
    grid-template-columns: repeat(3, 1fr);
  }
`;

export const NewsLetterTextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media only screen and (max-width: ${tablet_lg}px) {
    align-items: center;
  }

  @media only screen and (min-width: ${desk_xs}px) {
    max-width: 200px;
  }

  h3 {
    font-size: 23px;
    text-transform: uppercase;
  }

  p {
    font-size: 12px;
    color: ${greyDark()};
  }
`;

export const NewsLetterForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media only screen and (max-width: ${tablet_lg}px) {
    margin-top: 20px;
  }

  input, button {
    padding: 10px 15px;
  }

  input {
    font-size: 12px;
    flex: 1 1 100%;
    border: 0;

    &::placeholder {
      color: ${grey};
      text-transform: uppercase;
    }
  }

  button {
    width: 100px;
    color: ${secondary};
    font-size: 10px;
    font-weight: 700;
    text-transform: uppercase;
    background-color: ${primary};
  }

  > div {
    display: flex;
    padding-top: 10px;
    align-items: center;
  }
`;

export const LinksContainer = styled.div`
  padding: 10px;
  color: ${secondary};
  background-color: ${primary};

  @media only screen and (min-width: ${desk_xs}px) {
    display: grid;
    padding: 15px;
    grid-template-areas: 'links links . .';
  }


  @media screen and (min-width: ${desk_lg}px) {
    grid-template-areas: 'links links . . . .';
  }
`;

export const LinksList = styled.ul`
  display: flex;
  grid-area: links;
  justify-content: space-around;
  align-items: center;
  font-size: 10px;
  text-transform: uppercase;
  list-style: none;

  @media only screen and (max-width: ${mobile_lg}px) {
    flex-wrap: wrap;
    flex-direction: column;
    align-items: flex-start;
  }

  li {
    padding: 10px;

    &.icon {
      @media only screen and (max-width: ${mobile_lg}px) {
        display: none;
      }
    }
  }
`;

export const CorporateInfos = styled.div`
  display: flex;
  padding: 15px;
  justify-content: center;
  align-items: center;

  p {
    font-size: 7px;
    color: ${greyDark()};
    text-transform: uppercase;
  }
`;
