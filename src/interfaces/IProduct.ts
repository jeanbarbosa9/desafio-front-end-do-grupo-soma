export interface Installment {
  Value: number;
  InterestRate: number;
  NumberOfInstallments: number;
}

interface Seller {
  commertialOffer: {
    Installments: Installment[];
    Price: number;
    ListPrice: number;
    AvailableQuantity: number;
  };
}

export interface Item {
  itemId: string;
  nameComplete: string;
  variations: string;
  Tamanho: string;
  referenceId: [{
    key: string;
    value: string;
  }];
  images: [{
    imageUrl: string;
  }];
  sellers: Seller[];
}

export default interface ProductProps {
  productId: string;
  productName: string;
  productReference: string;
  categoryId?: string;
  categories?: string[];
  categoriesIds?: string[];
  linkText: string;
  items: Item[];
  sizeInShelf?: number;
  "Composição"?: string;
  "Tabela de Medidas"?: string[];
  description?: string;
}
