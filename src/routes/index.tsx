import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { ProductsListProvider } from '../hooks/productsList';

import Home from '../pages/Home';
import Category from '../pages/Category';
import Product from '../pages/Product';

const Routes: React.FC = () => (
  <Switch>
    <Route path='/' exact component={Home} />
    <ProductsListProvider>
      <Route path='/novidades' component={Category} />
      <Route path='/colecao' component={Category} />
      <Route path='/joias' component={Category} />
      <Route path='/sale' component={Category} />
      <Route path='/inside' component={Category} />
      <Route path='/:productLink/p' component={Product} />
    </ProductsListProvider>
  </Switch>
);

export default Routes;
