interface pathsDictionaryProps {
  [key: string]: string;
}

const pathsDictionary: pathsDictionaryProps = {
  colecao: 'coleção',
  novidades: 'novidades',
  joias: 'jóias',
  sale: 'sale',
  inside: 'inside',
}

export default pathsDictionary;
