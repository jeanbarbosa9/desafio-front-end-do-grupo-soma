import { Item } from '../interfaces/IProduct';

const getBestItemInstallment = (item: Item) =>
  item && item.sellers && item.sellers[0].commertialOffer.Installments.reduce(
    (accumulator, current) => {
      if (current.NumberOfInstallments > accumulator.NumberOfInstallments) {
        accumulator.Value = current.Value;
        accumulator.NumberOfInstallments = current.NumberOfInstallments;
        accumulator.InterestRate = current.InterestRate;
      }

      return accumulator;
    }, { Value: 0, NumberOfInstallments: 1, InterestRate: 0 }
  );

export default getBestItemInstallment;
