# Desafio Front-End - Grupo SOMA

Este repositório foi criado para atender aos requisitos do desafio do Grupo SOMA, para a vaga de Front-End.

Para facilitar a visualização da entrega e ter uma experiência melhor de visualização pelo celular (sem a necessidade de rodar o projeto localmente), o projeto foi disponibilizado através da seguinte URL http://desafio-grupo-soma.surge.sh/.

Para rodar o projeto localmente, siga as instruções da sessão `Rodando o projeto localmente`.

## Escopo do desafio
![banner inicial](./readme_assets/banner_inicial.png)

### Instruções para o teste
- Dê um fork neste projeto;
- Desenvolva as telas dos arquivos da pasta layout;
- Atualize o readme com as instruções necessárias para rodar o seu código;
- Faça um pull request.

### Sugestões de implementação
- Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)
- Filtro de produtos funcional
- Adicionar produtos ao carrinho
- Botão de carregar mais produtos

### Dicas
- Evite usar linguagens, ferramentas e metodologias que não domine;
- Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;

### Dúvidas
Entre em contato com vinicius.diniz@somagrupo.com.br

## Tecnologias utilizadas
Este projeto utiliza a renderização através do React em conjunto com TypeScript e Styled Components. No React foi utilizada a API de hooks e Context, nativas nas versões mais recentes da biblioteca. A opção de usar o TypeScript foi devido ao fato de deixar a leitura do código mais clara, a nível de declaração de propriedades de cada componente ou método e, o Styled Components porque deixa os elementos mais limpos sem a necessidade de uso de muitos atributos, fora toda a flexibilidade de utilizar lógica e variáveis do JavaScript, dentro do css e ainda a possibilidade de compartilhá-las com os componentes. Para a consulta via API foi utilizado o Axios, apenas por questões de praticidade.

## Dependências globais para rodar o projeto
- Git;
- Node (dependências instaladas na versão 12);
- Yarn ou NPM;

## Rodando o projeto localmente
1. Clone este repositório;
2. Através do terminal, navegue até a pasta onde este projeto foi instalado na sua máquina;
3. Rode o comando `Yarn` ou `NPM install`, para instalar as dependências;
4. Em seguida rode o projeto com `yarn start` ou `npm run start`;

## Funcionalidades implementadas
### Layout responsivo baseado no layout desk;
![responsividade home](./readme_assets/responsividade-home.gif)

### Navegação entre páginas usando React Router Dom, com breadcrumb dinâmico de acordo com a página ou categoria do produto;
![navegação entre páginas](./readme_assets/navegacao-paginas.gif)

### Interação de hover nos elementos de menu, imagem na listagem de produtos, botões e seletores;
![interações de hover](./readme_assets/interacoes-hover.gif)

### Listagem de produtos através da leitura de JSON mocado, com alteração de itens por linha na listagem de produtos;
![alteração na visualização de produtos da vitrine](./readme_assets/alteracao-itens-por-linha.gif)

### Filtro de produtos funcional;
![filtro de categoria e tamanho funcionais](./readme_assets/filtro-de-categoria-tamanho.gif)

### Ordenação de produto funcional;
![oderação de produtos funcional](./readme_assets/ordenacao-produtos.gif)

### Placeholder para representação de loading da página;
![carregamento de tela com placeholder](./readme_assets/carregamento-com-placeholder.gif)

### Paginação e carregamento de mais produtos na listagem de produtos;
![paginação com carregamento de mais produtos](./readme_assets/paginacao-carregamento-mais-produtos.gif)

### Transições de imagens na página de produto mobile;
![visualização diferenciada das imagens dos produtos no mobile](./readme_assets/visualizacao-imagens-mobile.gif)

### Adição de produtos ao carrinho com verificação de existência do item para interação de quantidade ou inclusão de novo item;
![adição de produtos ao carrinho mobile](./readme_assets/adicao-produtos-carrinho.gif)
![adição de produtos ao carrinho desk](./readme_assets/adicao-produtos-carrinho-desk.gif)

### Visualização de detalhes do produto;
![visualização de detalhes e composição do produto](./readme_assets/descricao-e-detalhes-do-produto.gif)
